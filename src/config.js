module.exports = {
  port: process.env.PORT,
  NODE_ENV: process.env.NODE_ENV,
  DEBUG: process.env.DEBUG,
  elasticSearchURL: process.env.ELASTIC_SEARCH_URL,
  profileApiURL: process.env.PROFILE_API_URL,
  modelApiURL: process.env.MODEL_API_URL,
  rabbitMqURL: process.env.RABBIT_MQ_URL,
  notificationsApiURL: process.env.NOTIFICATIONS_API_URL,
  apolloEngine: {
    apiKey: process.env.APOLLO_ENGINE_API_KEY
  },
  mails: [process.env.ADMIN_MAIL, process.env.QA_MAIL],
  redis: {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    password: process.env.REDIS_PASSWORD,
    expirationTime: eval(process.env.REDIS_EXPIRATION_TIME),
    log: process.env.REDIS_LOG != 0,
    useRedis: process.env.USE_REDIS
  },
  adminPassword: process.env.ADMIN_PASSWORD,
  adminSecretToken: process.env.ADMIN_SECRET_TOKEN,
  alert: {
    url: process.env.ALERT_MANAGER_URL,
    username: process.env.ALERT_MANAGER_USER,
    password: process.env.ALERT_MANAGER_PASSWORD,
    level: process.env.ALERT_MANAGER_LEVEL
  },
  messages: {
    resourceProtectedWithLoginError:
      "You must be signed in to view this resource",
    NOT_FOUND: "URL_NOT_FOUND",
    ELASTIC_SEARCH_NOT_FOUND_500:
      "Connection with elastic search couldn't be achieved",
    REST_API_200: "XXX returned OK",
    REST_API_300: "XXX returned REDIRECT",
    REST_API_400: "XXX returned UNAUTHORIZED",
    REST_API_ERROR_500: "XXX returned SERVER FAULT"
  },
  dateTimeFormat: process.env.DATE_TIME_FORMAT,
  usersMaxfetch: process.env.USERS_MAX_FETCH
};
