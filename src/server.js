try {
  console.log("Trying to load extensions.....");
  require.resolve("@reindex/plugins");
} catch (e) {
  console.log("Couldnt find extensions.... quitting");
  process.exit(e.code);
}
console.log("Found Extensions successfully bootstrapping....");

const config = require("./config");
const express = require("express");
const bodyParser = require("body-parser");
const { createServer } = require("http");
let server = require("./api");
const cookieParser = require("cookie-parser");
const winston = require("winston");
const expressWinston = require("express-winston");
const Logger = require("./utils/logger");

const app = express();
app.use(bodyParser.json({ limit: "100mb" }));
app.use(cookieParser());

// winston requests log
app.use((req, res, next) => {
  if (config.DEBUG >= 3)
    expressWinston.logger({
      transports: [new winston.transports.Console()],
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json()
      )
    });
  return next();
});

app.use(require("./utils/routes/index")); // supportive REST routes

server = server.createApolloServer(app);

const httpServer = createServer(app);

server.installSubscriptionHandlers(httpServer);
httpServer.listen({ port: config.port }, () => {
  console.log({
    label: "Server",
    message: `Apollo Server started at ${config.port}`
  });
});

//respond with json body for 404 status
app.use((req, res, next) => {
  res.status(404).json({
    error: "not found"
  });
});

// winston error logs
app.use((req, res, next) => {
  if (config.DEBUG >= 1)
    expressWinston.errorLogger({
      transports: [new winston.transports.Console()],
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json()
      )
    });
});

//respond with json body for internal errors
app.use((err, req, res, next) => {
  res.status(err.status || 500).json({
    error: err.message
  });
  next(err);
});
