const {
  DEBUG,
  NODE_ENV,
  alert: { level }
} = require("../config");

const { sendAlert } = require("./alert");

const winston = require("winston");

const sharedFormat = winston.format.printf(
  ({ level, message, label, timestamp }) => {
    return `[${timestamp} / ${level} / ${label.toUpperCase()}] : ${message}`;
  }
);

const Logger = winston.createLogger({
  level: "info",
  defaultMeta: { serviceName: "graph" },
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.label({ label: process.env.SERVICE_NAME }),
        winston.format.timestamp(),
        winston.format.colorize(),
        sharedFormat
      )
    })
  ]
});

console.log = function() {
  return Logger.info.apply(Logger, arguments);
};
console.warn = function() {
  return Logger.warn.apply(Logger, arguments);
};
console.error = function() {
  sendAlert(arguments);
  return Logger.error.apply(Logger, arguments);
};

module.exports = Logger;
