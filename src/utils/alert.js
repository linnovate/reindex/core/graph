const { url, username, password } = require("../config").alert;
const request = require("request");

const sendAlert = alert => {
  if (!url) return;
  const options = {
    method: "POST",
    auth: {
      user: username,
      pass: password,
      sendImmediately: true
    },
    headers: {
      "content-type": "application/json"
    },
    json: JSON.stringify({
      ...alert
    })
  };
  try {
    request.post(url, options, (err, res, body) => {
      if (err)
        console.error({
          label: "Alert Error",
          message: "Couldnt send message to alert manager"
        });
      if (res.statusCode > 400)
        console.error({
          label: "Alert Error",
          message: "Alert manager recieved error from alert server"
        });

      return console.info({
        label: "Alert Succeed",
        message: "successfully sent alert"
      });
    });
  } catch (err) {}
};

module.exports.sendAlert = sendAlert;
