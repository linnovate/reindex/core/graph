const { merge, extend } = require("lodash");
const { makeExecutableSchema } = require("graphql-tools");
const GraphQLJSON = require("graphql-type-json");
// root type defs
const rootTypeDefs = require("./root.schema.defenitions");
// Extensions typedefs
const { graph } = require("@reindex/plugins");
if (!graph) throw "Couldnt find Graph Object in extensions";
const {
  extensionsRootTypeDef,
  extensionsTypeDef,
  extensionsResolvers,
  extensionsDirectives
} = graph;
if (!extensionsTypeDef && !extensionsResolvers) {
  throw "Couldn't find any TypeDef or Resolver from extensions";
}

const expandedResolvers = merge({ JSON: GraphQLJSON }, ...extensionsResolvers);
const expandedDirectives = extend({}, ...extensionsDirectives);

const executableSchema = makeExecutableSchema({
  typeDefs:
    extensionsTypeDef != []
      ? [...extensionsRootTypeDef, rootTypeDefs, ...extensionsTypeDef]
      : null,
  resolvers: expandedResolvers || {},
  schemaDirectives: expandedDirectives || {},
  formatError: error => {
    return new Error("Internal server error " + error.message);
    // Or, you can delete the exception information
    // delete error.extensions.exception;
    // return error;
  }
});

module.exports = executableSchema;
